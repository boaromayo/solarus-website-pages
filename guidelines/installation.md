# Installing the website

## Installing a local server

We strongly recommend to test the modifications you make in an actual environment. Modifying files without seeing the result is bad practice. For this, you will need to setup a local web server.

### Windows

You may use WAMP, which stands for "Windows Apache MySQL PHP". [Download it](http://www.wampserver.com/) and install it. It will, by default, be installed into `C:\wamp`.

Start WAMP from the Start menu, and when it's started, you can access your website in your browser, by typing `localhost`in the address bar.

### MacOS

You may use MAMP, which stands for "Mac Apache MySQL PHP". [Download it](https://www.mamp.info) and install it.

MAMP allows you to modify the folder that will be used by your server. We recommend not using the default one, which is in `applications` but rather setting it to where you store your development projects (`~/Documents/Development/` for instance). You can modify this in the menu **MAMP > Preferences > Web Server > Documents Root**.

### Linux

These instructions are suitable for local development, but not deployment into production.

1. Install Apache2, PHP7, and required dependencies:
```
sudo apt install apache2 php7.0 php7.0-mbstring composer
```
2. Add yourself to www-data group:
```
sudo usermod -aG www-data $(whoami)
```
3. Log out and log back in (so your new group is set).
4. Enter Apache2's root directory:
```
cd /var/www
```
5. Set permissions:
```
sudo chown -R www-data:www-data .
sudo chmod 775 .
```
6. Clone the repos:
```
git clone https://gitlab.com/solarus-games/kokori.git
git clone https://gitlab.com/solarus-games/solarus-website-pages.git kokori/public/data
```
7. Run composer:
```
cd kokori
composer install
```
8. Set permissions, again:
```
sudo chmod -R 775 application/cache
sudo chmod -R 775 application/logs
```
9. Edit `/etc/apache2/sites-enabled/000-default.conf` and change `DocumentRoot /var/www/html` to `DocumentRoot /var/www/kokori/public`:
```
sudo nano /etc/apache2/sites-enabled/000-default.conf
```
10. Edit `/etc/php/7.0/apache2/php.ini` and change `short_open_tag = Off` to `short_open_tag = On` (find the line that's already uncommented):
```
sudo nano /etc/php/7.0/apache2/php.ini
```
11. Restart services:
```
sudo service php7.0-fpm restart
sudo service apache2 restart
```
12. Visit `http://localhost` in your browser. The homepage should work now! The other pages don't.
13. Enable mod_rewrite:
```
sudo a2enmod rewrite
sudo service apache2 restart
```
14. You'll need to edit the Apache config once again:
```
sudo nano /etc/apache2/sites-enabled/000-default.conf
```
After the opening VirtualHost tag (above DocumentRoot) include this snippet:
```
<Directory /var/www/kokori>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Order allow,deny
    allow from all
</Directory>
```
[See here](https://askubuntu.com/questions/48362/how-to-enable-mod-rewrite-in-apache) if you have trouble with this step.

15. You're done, congrats! Try visting `localhost` in your browser again and you should be able to navigate any page.

## Installing Git

You need to install [Git](https://git-scm.com/) to contribute to the website. Git is a free and open source distributed version control system.

Follow the installation instructions for the platform you are working on.

Git is originally intended to be used with a terminal, but some graphical apps exist. However, the terminal allows for more control, and we recommend using it.

**NB:** On Windows, the native console is not well suited for using Git. So the Git installation also provides Git Bash, a console that looks like a Unix terminal, just like on Linux and MacOS. Use this one.

## Downloading the website

Open your terminal, and go into the folder where your server will look up the files.

```bash
cd path/to/your/server
```
**NB:** On Windows, it's in `C:/wamp/www/` by default. You may want to configure WAMP to place it elsewhere. See the section below.

There, we will download first the [website engine](https://gitlab.com/solarus-games/kokori). First, fork the project `kokori`: it means you create your own copy, since you cannot modify directly ours.

Then, clone it to your computer. It will download a local copy to your computer, in a folder named `kokori`.

```bash
git clone https://gitlab.com/your-name/kokori
```

The engine serves pages that are located in its `public/data` folder. Currently, at this step, the `data` folder is tempty. This is where we will download the actual pages.

Then, fork [the pages](https://gitlab.com/solarus-games/solarus-website-pages), i.e. the repository `solarus-website-pages`, and clone it into a folder named `data`:

```bash
git clone git@gitlab.com:your-name/solarus-website-pages.git data
```

Now, you should have both the engine and the pages on your computer.

### Configuring WAMP on Windows

You need to edit the `httpd-vhosts.conf` file to ensure Apache will point directly to `kokori/public` folder. 
* Click on the WAMP icon in the system tray. It'll open a menu.
* Go into **Apache > Edit httpd-vhosts.conf**.
* Modify the `DocumentRoot` variable (default is `C:/wamp/www/`) to point to your `kokori/public` path.
* Add missing configuration settings.
* Save file.
* Click on the WAMP icon i the system tray and click on **Restart All Services**.

```xml
# Virtual Hosts
#
<VirtualHost *:80>
  ServerName localhost
  ServerAlias localhost
  DocumentRoot "C:/path/to/kokori/public"
  <Directory "C:/path/to/kokori/public">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
    Order allow,deny
    allow from all
  </Directory>
</VirtualHost>

```

## Testing the website

Ensure that your server (WAMP, MAMP or any other way), and open a web browser. Type `localhost`in the address bar (`localhost:8888` for a non-Windows OS). You should see the website in your browser.

Modifying a file needs the browser to be refreshed to see the changement.
