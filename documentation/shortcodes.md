[container]
#Shortcodes

This page aims to **describe** and **list** all the shortcodes you can use when writing a page.
Know first that **you can combine shortcodes with markdown and/or tags**. The only restriction is that you can not nest a shortcode in another shortcode of the same name. To work around this, you can use aliases.

For example, `alert_1` is an alias for `alert`.
Furthermore, you can create as many aliases as you want.

##Alert

* **Name:** alert
* **Use on the site:** All over
* **Description:** Display a message as an alert.
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **dismiss**
        * **Mandatory:** No
        * **Possible values:** true|false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `dismiss="true"`
    * **icon**
        * **Mandatory:** No
        * **Possible values:** Fontawesome icon slug or false
        * **Default value:** false
        * **Conditions of use:** None
        * **Note:** 
        By using this attribute, you can by extension use all the attributes related to the icon shortcode by prefixing them with icon-.
        * **Sample:** `icon="home"`
    * **title**
        * **Mandatory:** No
        * **Possible values:** Arbitrary string
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `title="Alert title"`
    * **type**
        * **Mandatory:** No
        * **Possible values:** primary|secondary|success|danger|warning|info|light|dark
        * **Default value:** primary
        * **Conditions of use:** None
        * **Sample:** `type="primary"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

##Badge

* **Name:** badge
* **Use on the site:** All over
* **Description:** Display a small badge.
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **icon**
        * **Mandatory:** No
        * **Possible values:** Fontawesome icon slug or false
        * **Default value:** false
        * **Conditions of use:** None
        * **Note:** 
        By using this attribute, you can by extension use all the attributes related to the icon shortcode by prefixing them with icon-.
        * **Sample:** `icon="home"`
    * **type**
        * **Mandatory:** No
        * **Possible values:** primary|secondary|success|danger|warning|info|light|dark
        * **Default value:** primary
        * **Conditions of use:** None
        * **Sample:** `type="primary"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

##Button

* **Name:** button
* **Use on the site:** All over
* **Description:** Display a button 
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **icon**
        * **Mandatory:** No
        * **Possible values:** Fontawesome icon slug or false
        * **Default value:** false
        * **Conditions of use:** None
        * **Note:** 
        By using this attribute, you can by extension use all the attributes related to the icon shortcode by prefixing them with icon-.
        * **Sample:** `icon="home"`
    * **link**
        * **Mandatory:** No
        * **Possible values:** Absolute/relative url or false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `link="http://www.zelda-solarus.com"`
    * **outline**
        * **Mandatory:** No
        * **Possible values:** true|false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `outline="true"`
    * **size**
        * **Mandatory:** No
        * **Possible values:** sm|md|lg
        * **Default value:** md
            * **Conditions of use:** None
        * **Sample:** `size="lg"`
    * **target**
        * **Mandatory:** No
        * **Possible values:** _parent|_blank
        * **Default value:** _parent
        * **Conditions of use:**
            * The "link" parameter must not be empty
        * **Sample:** `target="_blank"
    * **type**
        * **Mandatory:** No
        * **Possible values:** primary|secondary|success|danger|warning|info|light|dark
        * **Default value:** primary
        * **Conditions of use:** None
        * **Sample:** `type="primary"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

##Column

* **Name:** column
* **Use on the site:** All over
* **Description:** Specify a new container column
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **width**
        * **Mandatory:** No
        * **Possible values:** Integer from 1 to 12
        * **Default value:** auto
        * **Conditions of use:** None
        * **Sample:** `width="6"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

## Container
-----------------

* **Name:** container
* **Use on the site:** All over
* **Description:** Specify a new container grid
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **is_fluid**
        * **Mandatory:** No
        * **Possible values:** true|false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `is_fluid="true"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

##Cover

* **Name:** cover
* **Use on the site:** All over
* **Description:** Specify a new container cover
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

## Icon

* **Name:** icon
* **Use on the site:** All over
* **Description:** Display an icon
* **Content:** No
* **Mandatory content:** No
* **Parameters:**
    * **icon**
        * **Mandatory:** Yes
        * **Possible values:** Fontawesome icon slug or false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `icon="home"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`
        
## Jumbotron

* **Name:** jumbotron
* **Use on the site:** All over
* **Description:** Display a jumbotron
* **Content:** Yes
* **Mandatory content:** No
* **Parameters:**
    * **title**
        * **Mandatory:** Yes
        * **Possible values:** Arbitrary string
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `title="Jumbotron title"`
    * **lead**
        * **Mandatory:** No
        * **Possible values:** Arbitrary string
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `lead="Jumbotron lead"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`
        
## Model

* **Name:** model
* **Use on the site:** All over
* **Description:** Display and translate a model
* **Content:** No
* **Mandatory content:** No
* **Parameters:**
    * **id**
        * **Mandatory:** Yes
        * **Possible values:** Arbitrary string
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `id="model_name"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

## Paypal

* **Name:** paypal
* **Use on the site:** All over
* **Description:** Display a donate button with paypal
* **Content:** No
* **Mandatory content:** No
* **Parameters:**
    * **email**
        * **Mandatory:** Yes
        * **Possible values:** Arbitrary email
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `email="email@domain.fr"`
    * **name**
        * **Mandatory:** Yes
        * **Possible values:** Arbitrary string
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `name="My name"`
    * **currency**
        * **Mandatory:** Yes
        * **Possible values:** Arbitrary money
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `currency="EUR"`
    * **label**
        * **Mandatory:** Yes
        * **Possible values:** Arbitrary string
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `label="Donate"`
    * **type**
        * **Mandatory:** No
        * **Possible values:** primary|secondary|success|danger|warning|info|light|dark
        * **Default value:** primary
        * **Conditions of use:** None
        * **Sample:** `type="primary"`
    * **icon**
        * **Mandatory:** No
        * **Possible values:** Fontawesome icon slug or false
        * **Default value:** false
        * **Conditions of use:** None
        * **Note:** 
        By using this attribute, you can by extension use all the attributes related to the icon shortcode by prefixing them with icon-.
        * **Sample:** `icon="home"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

## Progressbar

* **Name:** progressbar
* **Use on the site:** All over
* **Description:** Display a progressbar
* **Content:** Yes
* **Mandatory content:** No
* **Parameters:**
    * **height**
        * **Mandatory:** Yes
        * **Possible values:** Integer
        * **Default value:** 20
        * **Conditions of use:** None
        * **Sample:** `height="30"`   
    * **width**
        * **Mandatory:** Yes
        * **Possible values:** Integer
        * **Default value:** 100
        * **Conditions of use:** None
        * **Sample:** `width="100"`
    * **type**
        * **Mandatory:** No
        * **Possible values:** primary|secondary|success|danger|warning|info|light|dark
        * **Default value:** primary
        * **Conditions of use:** None
        * **Sample:** `type="primary"`
    * **striped**
        * **Mandatory:** No
        * **Possible values:** true|false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `striped="true"`     
    * **animated**
        * **Mandatory:** No
        * **Possible values:** true|false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `animated="true"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`
            
## Row

* **Name:** row
* **Use on the site:** All over
* **Description:** Specify a new row grid
* **Content:** No
* **Mandatory content:** No
* **Parameters:**
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

## Space

* **Name:** space
* **Use on the site:** All over
* **Description:** Display a space
* **Content:** No
* **Mandatory content:** No
* **Parameters:**
    * **height**
        * **Mandatory:** No
        * **Possible values:** height in pixels
        * **Default value:** 10
        * **Conditions of use:** None
        * **Sample:** `height="20"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`
        
## Summary

* **Name:** summary
* **Use on the site:** All over
* **Description:** Display a summary
* **Content:** No
* **Mandatory content:** No
* **Parameters:**
    * **level-max**
        * **Mandatory:** No
        * **Possible values:** integer|false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `level-max="2"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`
        
## Table

* **Name:** table
* **Use on the site:** All over
* **Description:** Specify a new container table
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

## Table column

* **Name:** table-row
* **Use on the site:** All over
* **Description:** Specify a new row table
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **colspan**
        * **Mandatory:** No
        * **Possible values:** integer|false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `colspan="2"`
    * **rowspan**
        * **Mandatory:** No
        * **Possible values:** integer|false
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `rowspan="2"`  
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

## Table row

* **Name:** table-row
* **Use on the site:** All over
* **Description:** Specify a new row table
* **Content:** Yes
* **Mandatory content:** Yes
* **Parameters:**
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`

## Youtube

* **Name:** youtube
* **Use on the site:** All over
* **Description:** Display a youtube video 
* **Content:** No
* **Mandatory content:** No
* **Parameters:**
    * **id**
        * **Mandatory:** Yes
        * **Possible values:** Youtube id
        * **Default value:** false
        * **Conditions of use:** None
        * **Sample:** `id="7GR308Ox-8o"`
    * **allowfullscreen**
        * **Mandatory:** No
        * **Possible values:** true|false
        * **Default value:** true
        * **Conditions of use:** None
        * **Sample:** `allowfullscreen="false"`
    * **effect-in**
        * **Mandatory:** No
        * **Possible values:** 
        * **Default value:** Effect name|false
        * **Conditions of use:** None
        * **Sample:** `effect-in="fade"`
[/container]