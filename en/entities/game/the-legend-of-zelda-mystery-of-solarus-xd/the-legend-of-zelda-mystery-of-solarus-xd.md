### Presentation

*The Legend of Zelda: Mystery of Solarus XD* is a parodic game that we released on April 1st, 2011. Though it’s a big April 1st joke, it’s a real, full game with two huge dungeons and 5-10 hours of playing.

It was developed in a very short period of time, thanks to coffee, beer and pizzas. Though the development has been rushed to ship on time, the game is complete, with lots of jokes, references and quirky NPCs.

![Link](artworks/artwork_link.png "Link")

### Synopsis

As in many *Legend of Zelda* games, your adventure begins by Link waking up. Though today, Link wakes up with no memory of what happened the previous night. It seems like he has drunk too much: he has lost his sword, his shield, and more important: he has lost track of Princess Zelda! Where is she? What happened to her? Was she present the previous night too?

And so begins your journey... or, to be more precise, your epic hangover.

![Zelda](artworks/artwork_zelda.png "Zelda")
