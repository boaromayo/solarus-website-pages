### Synopsis

ZeldoRetro made a short *The Legend of Zelda* game in 2017 to challenge his friend Adenothe. It was not aimed to be publicly released. However, the release of the second chapter made him consider releasing the first one, because everyone was asking "Wait? If this is chapter 2, where is chapter one?". Here it is !

This game is then the first entry in ZeldoRetro's series *Le Défi de Zeldo* (Zeldo's Challenge). This first chapter is named *La Revanche du Bingo* (Bingo's Revenge), and is much less ambitious than its sequel. It contains only one dungeon, and no overworld to explore, which makes the game rather short. You may beat it under an hour.
