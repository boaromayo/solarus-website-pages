[container]

# News

[row]
[column width="8"]
[article-listing read-more-label="Read more..."]
[/column]

[column width="4"]
[box-highlight]

## Filters

[entity-filter entity-type="article" filter="categories" title="Categories"]
[entity-filter entity-type="article" filter="authors" title="Authors"]
[/box-highlight]
[/column]
[/row]
[/container]