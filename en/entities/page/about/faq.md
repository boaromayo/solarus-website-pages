[container]

[row]
[column width="8"]

# {title}

## About Solarus

### What is Solarus?

Solarus is a **free and open-source game engine**, licensed under GPL v3. It is **written from scratch in C++** and uses SDL2. Consequently, it is available on a large set of machines. It’s not an emulator neither a Super Nintendo™ ROM hack.

Development began in 2006. The goal of the engine is to **allow people to create their own Action-RPG games** (also called Zelda-like games). It is totally independent from Nintendo.

**Solarus does not contain any copyrighted code or assets**: it is 100% free and open-source. Thus, Solarus cannot be eligible for a DMCA from anyone, including Nintendo.

Solarus is not a "Zelda engine" as can be read sometimes; it can power any Action-RPG or Action-Adventure game (or any kind of game you want, with a little more work). *A Link To The Past* assets (tilesets, sprites, musics, etc.) are totally separated from the engine (the engine does not contain them).

### What is Solarus Quest Editor?

Solarus Quest Editor is a free and open-source **game editor** to create quests for Solarus. Written in **C++ with Qt**, it is licensed under GPL v3. The goal of the editor is to provide people a smooth experience in creating a game with Solarus. This software provides a map editor, a tileset editor, a sprite editor, a Lua script editor, among other things.

### What is a Solarus Quest?

A Solarus quest is simply a game made with Solarus.

### Why is there several exectuable files in my download package?

Theses files are:

* **Solarus Launcher:** the graphical app used for managing your library of games (quests in Solarus terms), and for lauching them.
* **solarus_run:** the command line app to run quests. It’s totally normal that is does nothing if you execute it without telling it which quest to run as a parameter.
* **Solarus Quest Editor:** the quest editor (graphical app).

### What is possible to do with Solarus?

We're often asked questions like 'Can I do this with Solarus?'. The answer is probably yes, you can.

Solarus has a Lua API, and you can program whatever you want in Lua. Consequently, getting something to work in Solarus is almost always possible if you are willing to put in the work. However, the engine does not handle 3D graphics: this is the only limitation. Any kind of game is possible.

## About licenses

### Are you affiliated with Nintendo?

**No, we are independent from Nintendo.** The game engine Solarus has no proprietary content and is written to be a general purpose action-RPG game engine, i.e. not only *The Legend of Zelda* games.

However, **some games made with Solarus are using copyrighted assets by Nintendo**, mainly from *The Legend of Zelda: A Link To The Past*. This is for historical reasons and we make our possible to create Creative Commons assets to replace them. To be clear, historically, Solarus began its life as a simple engine to power a fangame for the *Zelda* series: *Mystery Of Solarus DX*. We believe this is fair use.

### Can I use Solarus for a commercial game?

First, please know that your game will be divided into 2 parts:

* **The engine:** Solarus (that you modify or use as-is).
* **The quest:** the game data (maps, scripts, sprites, dialogs, etc.).

**The engine is GPL**, so it requires to follow some rules:

* If you use Solarus as-is: please provide a link to Solarus source code.
* If you modify Solarus and release publicly your game, you must provide access to your modified Solarus source code. Moreover, this modified version is automatically licensed under GPL too.

**You game data may have whatever license you want**. It may be different than GPL and remain private code/assets. Commercial licenses are thus allowed for your quest.

* Brand-new assets/scripts made by you can be whatever license you want.
* Reused or modified assets/scripts must follow the rules of their original license. Please take care for GPL Lua scripts or CC assets: they must also be GPL if modified. Note also that using proprietary assets is forbidden by law.

### Can I use Zelda sprites, characters and musics?

**For a commercial game, obviously: NO.** Don’t use assets that are copyrighted by anyone else, including Nintendo (sprites, characters, music, storyline…). Or you expose yourself to legal problems. You cannot say we didn't warn you.

**for a non-commercial game**, yes but keep it small. This is what we do. Unless Nintendo tells you to stop, this seems possible. Nintendo seems to tolerate non profit fan-made creations when they don’t get too big and prevent their own business, which is totally fair and logical.

We believe using *Zelda* assets as a basis to learn game dev is **fair use**. However, you should replace progressively these copyrighted assets with custom free ones, in order to not violate copyright.

## About games

### I downloaded a game, but it does not work. Why?

There are several possibilities, but here are the most common ones:

#### You might have mistaken the engine and the quest

You might have mistaken the engine and the quest, which are different piece of software.

To launch a quest, you have multiple possibilities:

* **From the terminal:** Run solarus (i.e. the engine) and specify the data path (i.e. the quest) as an argument. You may also place the data and the engine in the same folder and run the engine: the data will be automatically detected and used by the engine. Type `solarus_run path/to/my-quest-name` in the terminal.

* You may also use **Solarus Launcher** which is the graphical app (better for those who don't like the command-line). Import your game in the library, select it and click on Play.

#### Your version of Solarus is not up-to-date

Verify that the **Solarus version of your data** (i.e. the quest) can be run by your **version of Solarus**. If not, update Solarus.

Note that the engine has retro compatibility. It means you can play a quest made for an older version of Solarus with a newer one.

### Do the games work on SNES (Super Nintendo)?

**No, they don’t**, even if *The Legend Of Zelda: Mystery of Solarus DX* looks a lot like *The Legend Of Zelda: A Link To The Past*. Solarus has not been written to work on Super Nintendo and no one has made a port yet. Solarus is not an emulator playing ROM hacks, it is an engine written from scratch.

## About the project

### How can I contribute to the project?

Thank you! See the [contribution page](/en/development/how-to-contribute).

### Is the project backed by a legal entity?

Yes, the legal entity that backs the project is the French Loi 1901 association [Solarus Labs](/fr/about/nonprofit-organization). It is a nonprofit organization. The association will receive all the donations made to the project, and these donations will be entirely used for the project and only for that.

### How can I contact Solarus Team?

If you have a request/question that is not here, and want a quicker answer:

* [The forums](http://forum.solarus-games.org/)
* [The Discord chat](https://discord.gg/yYHjJHt)
* [The contact page](/en/about/contact)
* [The team behind the project](/en/about/contributors) if you want to specifically contact someone.

[/column]

[column width ="4"]
[summary level-min="2" level-max="3"]
[/column]

[/row]

[/container]
