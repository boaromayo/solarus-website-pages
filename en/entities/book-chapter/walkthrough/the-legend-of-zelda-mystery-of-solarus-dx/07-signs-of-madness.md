# Chapter 7: The signs of madness

With the Hookshot, new horizons are offered to you. Start by going to buy the **Apple Pie** at the village pastry shop for 35 rupees. Buy healing items or ammo until you have less than 200 rupees, so you can collect **100 rupees** from a chest in Hyrule Castle (this is your last chance to visit before you history does not make it different ...). It is in the dungeons (the entrance is to the right of the castle, under the last bush of a series of four). Bomb the south wall and you will access this famous chest.

Before entering the next dungeon, you can only stay with around 230 rupees, which will be ample. Now that you are ready, go to the extreme south-east of the map, to the lake shop. For this, from the bakery, go to the screen at the bottom, then right, follow the dirt road to the south and east until you get to four holes forming a square. Here, cross through the grapple, recover the chest on the left (**20 rupees**), cross the bridge, enter the shop, go to the room north, destroy the wall, exit, and finally lift the stone blocking the passage to the chest containing **Piece of Heart #11**. Use the grapple to find yourself on the right side, then go see the grandfather in front of the locked house. Give him the Apple Pie, and he will open you to the house of the signs. If you played the game on Gameboy (Link's Awakening), you will roll in fetal position remembering painful memories.

The goal is simple, just read the signs and follow the directions. Here is the path given:

```
right, up, left, up,
left, up, right, down,
right, up, right, down,
right, up, left, down,
right, up, left, up,
right, up, left, down,
left, up, right, down,
right, up, right, end.
```

![Surprise Wall](img/house-cave/surprise-wall.png)

Here are some practical tips. In the first place, destroy all the grass: it will slow you down and it will make you more tired than anything else (you will recover from the blow, with one stone two shots). Second advice (advice can be taken separately, it is not an obligation): destroy the signs after each reading. A sign can not be reused, and if you have the opportunity to avoid confusion, put the odds on your side. In addition, it can open you shortcuts to do that. If you do not survive, follow the map with the reading order of the signs. And when a sign says a direction, you must take the same line as the sign, so the new sign is not related to your place but to the sign itself. After reading all the panels in the correct order, you get the **Gold Bars**.

Get out and cross the ravine on the right. Continue a little north, three stones block access to a cave. This is the lair of Billy the Bold. Enter, and do not break especially the vase (it will be useful to leave). Cling to the chest that you can not open for now, and give the ingots to the one-eyed. He will provide you with **Edelweiss**. Use the vase to start again (you can break it now, hooligan band). Return to the entrance of the first dungeon, and continue west to see a small hill. Plant the Edelweiss at the top, jump into the hole and follow the only path available in the shade (going up, you have to go twice up, then left, and up) and you can finally go out to the north to enter the fourth dungeon.
