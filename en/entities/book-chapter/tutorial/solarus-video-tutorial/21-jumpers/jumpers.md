# {title}

[youtube id="9_AWDGgfEx0"]

## Summary

- Making a ladder
- Working with jumper entities
- Making the player jump from a cliff (vertically and diagonally)
- How to ensure the player doesn't land inside a wall
- Making a multi-level dungeon map
- Making the player jump on a multi-level dungeon map

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
