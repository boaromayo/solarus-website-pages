# {title}

[youtube id="KZoQxbrZrJ4"]

## Summary

- Enemy killed sprite
- Placing an enemy on the map
  - Skeletor
- Having enemy drop a treasure when killed
- Creating an enemy script
  - Updating enemy sprite direction as it walks around

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
- [Useful resources to reproduce this chapter](https://www.solarus-games.org/tuto/en/basics/ep26_resources.zip)
