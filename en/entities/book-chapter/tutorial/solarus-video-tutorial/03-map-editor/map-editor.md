# {title}

[youtube id="3rpmpdr-_F0"]

## Summary

- Using the map editor
  - Shortcuts
- Create and modify tiles
  - Tilesets
  - Traversable and wall ground types
  - Layers
  - Resize tiles

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
