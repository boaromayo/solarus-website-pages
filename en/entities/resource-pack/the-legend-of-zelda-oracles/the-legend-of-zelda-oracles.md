### Presentation

*The Legend of Zelda: Oracle of Ages* and *Oracle of Seasons* were released in 2001 on Game Boy Color. In these complementary games, the player can respectively travel through time and control the seasons. The artistic direction is picked up from *The Legend of Zelda: Link's Awakening DX*, published earlier on Game Boy Color too, though sprites and tilesets were expanded and improved.

This resource pack has been initiated by Katsu but is currently in need of artists and developers to be completed. Join us on [the forum](http://forum.solarus-games.org/index.php/topic,1292.msg8091.html) to help!

![The Legend of Zelda: The Minish Cap box](images/box_art.png)
