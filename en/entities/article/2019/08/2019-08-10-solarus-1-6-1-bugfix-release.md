We have just published a bugfix release for Solarus, which is now at version **1.6.1**. This release mostly contains bug fixes and adjustements for the recent switch to OpenGL in 1.6.0.

Quest creators will also appreciate some new features, like the ability to
build a quest package from Solarus Quest Editor.

## Changelog

Here is the exhaustive list of all changes brought by Solarus 1.6.1.
This represents almost eight months of work.
Thank you std::gregwar, hhromic, ClueDrew et everybody who contributed to this release!

### Changes in Solarus 1.6

![Solarus logo](2019-08-10-solarus-1-6-1-bugfix-release/solarus_logo.png)

#### Engine changes

* Add `-force-software-rendering` switch to solarus-run (stdgregwar).
* Add argument to set mouse cursor visibility on start (#1263).
* Add option in CMake to support building with OpenGL ES (#1270).
* Add option in CMake to disable logging errors to file (#1276).
* Add simple performance counters to the engine (#1280).
* Add argument to set fullscreen mode on start (#1281).
* Add joypad buttons combo for quitting the engine (#1283).
* Add option in CMake to control building of unit tests (#1288).
* Add root build path to unit tests environment in Windows (#1291).
* Add git revision information to build and main loop startup (#1292).
* Add argument to set the joypad axis deadzone (#1293).
* Add debug warning if a surface is larger than 2048 x 2048 (#1294).
* Add support of carried object sprites with multiple directions (#1392).
* Fix reordering hero sprites (#1243).
* Fix crash when starting a hero movement from hero:on_state_changed() (#1354).
* Fix crash when changing the hero state while pulling a block (#1371).
* Fix crash when starting drowning after falling into a teletransporter (#1353).
* Fix crash when stairs are disabled at map start with dynamic tiles (#1366).
* Fix crash when a state finishes from a coroutine (#1374).
* Fix action command lost after showing a dialog in the pause menu (#1408).
* Fix hero being hurt while in spiral stairs (#1038).
* Fix stairs not activating in a custom state with fixed direction (#1364).
* Fix crash when pickable follow streams (#1361).
* Fix camera movement stuck on separators (#1351).
* Fix jumpers when the hero size is other than 16x16 (#1381).
* Fix blocks when the hero size is other than 16x16 (#1383).
* Fix blocks stuck on separators (#1356).
* Fix entity:overlaps(...) crashing when given bad collision mode (#1041).
* Fix straight movement progress reinitialized when setting speed/angle.
* Fix sol.menu.start allowing to start one menu multiple times (#1044).
* Fix GlRenderer for non-Android GLES 2.0 devices (#1267).
* Fix OpenGL compatibility issues.
* Fix FindOpengl complaining about GLVND vs GLX choice (#1320).
* Fix incorrect mapping from window coords to quest coords (#1340).
* Fix mouse and finger coordinates using a wrong window size (#1268).
* Fix restoring to windowed mode from fullscreen (#1269).
* Fix screen surface not being initialised in 640x480 video mode (#1273).
* Fix initialisation of screen surface without video window (#1274).
* Fix restoring from fullscreen mode in Windows (#1284).
* Fix warning when the default quest size is not supported by the quest (#1376). 
* Fix building with mingw-w64 in Linux (#1282).
* Fix detection of LuaJIT version in CMake module (#1277).
* Fix building GUI with mingw-w64 in Linux (#1285).
* Fix running the unit tests from the build root folder (#1289).
* Fix broken unit test that fails in Windows but not Linux (#1290).

#### Lua API changes

This release adds new features but does not introduce any incompatibility.

* Add methods game:get/set_transition_style() (#1368).
* Add methods state:get/set_can_use_teletransporter/switch/stream() (#1363).
* Make surface, text_surface and timer userdata types indexable (#1394).

### Changes in Solarus Quest Editor 1.6.0

![Solarus Quest Editor logo](2019-08-10-solarus-1-6-1-bugfix-release/sqe_logo.png)

* Allow to build a quest package (#431).
* Map editor: allow teletransporters to have any size multiple of 8 pixels.
* Map editor: improve performance of deleting multiple entities.
* Map editor: fix performance of changing entities layer (#454).
* Map editor: fix wrong selections after undoing changing layers.
* Tileset editor: fix performance of removing multiple patterns (#456).
* Sprite editor: fix persisting error message about missing source image (#451).
* Dialogs editor: fix line wrapping.
* Initial quest: remove proprietary file added by mistake.
* Add Force Software Rendering option to 'Running' section (stdgregwar).

### Changes in Zelda Mystery of Solarus DX 1.12.1

![Mystery of Solarus DX logo](2019-08-10-solarus-1-6-1-bugfix-release/mos_dx_logo.png)

* Fix typo in English texts (#128).
* Fix mistakes in German texts (#137).
* Fix deprecated function calls since Solarus 1.6 (#131).

### Changes in Zelda Mystery of Solarus XD 1.12.1

![Mystery of Solarus XS logo](2019-08-10-solarus-1-6-1-bugfix-release/mos_xd_logo.png)

* Fix torch displayed above the hero in dungeon 1 (#55).

### Changes in Zelda Return of the Hylian SE 1.2.1

![Return of the Hylian SE logo](2019-08-10-solarus-1-6-1-bugfix-release/roth_se_logo.png)

* Fix ghostly proprietor sprite displaying issue (#114).

### Changes in Zelda XD2 Mercuris Chess 1.1.1

![XD2 Mercuris Chess logo](2019-08-10-solarus-1-6-1-bugfix-release/zsxd2_logo.png)

* Fix Solarus format to 1.6.
* Fix getting stuck on a stone in water near Rupee Island (#141).
* Fix ghostly proprietor sprite displaying issue (#142).
