Since the beginning of Solarus, the main resource pack has been the *A Link To The Past* one. Why? Because Solarus was created for the game *Mystery of Solarus*, which was a sequel to *ALTTP*. Logically, this is currently the most wel-stocked resource pack, thanks to Medoc and other people. You surely have noticed that several games use this resource pack.

However, *ALTTP* is not the only top-down 2D *Zelda* game, and some people have begun to create resource packs for the other 2D games in the series. This is a breath of fresh air for those who start to get bored of the *ALTTP* look and feel!

Be aware that these resource packs are not completed, and only feature a few tilesets or sprites. Your help is welcome to progress in completing these resource packs.

### Resource pack for The Legend of Zelda (NES, 1986)

This is the work of Maloney and CopperKatana. You can follow the progress on [the forum](http://forum.solarus-games.org/index.php/topic,1365.0.html). They even go further by adding items, sounds and musics that come from more recent Zelda games! If programming is among your skills, they are looking for help for scripting enemies and items.

![Zelda 1](2019-08-14-zelda-resource-packs-in-the-making/screenshot-zelda-1.png)

### Resource pack for Oracle of Ages/Seasons (GBC, 2001)

This is the work of Katsu. Unfortunately, the project has been abandonned and is looking for motivated people to take it up. You can get more information on [the forum](http://forum.solarus-games.org/index.php/topic,1292.msg8091.html) and on [the resource pack page](/en/development/resource-packs/the-legend-of-zelda-oracles)

![Oracles](2019-08-14-zelda-resource-packs-in-the-making/screenshot-oracles.png)


### Resource pack for The Minish Cap (GBA, 2004)

The beloved pixel art masterpiece (seriously, look at this work of art!) finally has its resource pack! This is the work of GregoryMcGregerson, and he made great progress. Unfortunately, the project is on pause. The author put everything on [Github](https://github.com/GregoryMcGregerson/minish-resource-pack), so don't hesitate to fork and improve this resource pack. You can get more information on [the forum](http://forum.solarus-games.org/index.php/topic,1346.0.html) and on [the resource pack page](/en/development/resource-packs/the-legend-of-zelda-the-minish-cap)

![Minish Cap](2019-08-14-zelda-resource-packs-in-the-making/screenshot-minish-cap.png)

Let's hope we'll see less and less *ALTTP* clones with these new resource packs!
