Have you ever dreamed about wearing Solarus underpants?

Well, it is now possible. Solarus has now a clothing brand that features T-shirts, tank tops, hoodies, caps and even bags!

![tshirt](images/spreadshirt-shop.jpg)

All the benefits will go directly to the nonprofit organization [Solarus Labs](/en/about/nonprofit-organization) that we've created in January 2020. Money will be used to help Solarus, by paying the server's fees or by buying hardware you want Solarus to be ported to, for instance. We also plan to organize events like contests or meetings if money allow us to do so.

Admit that you would look much cooler with a Solarus Quest Editor cap, right?

Ah. And *yes*, you can actually buy your pair of real Solarus underpants.

[![boxer](images/boxer.jpg)](https://shop.spreadshirt.fr/solarus-labs/solarus+engine+logotype-A5e838b6b22250978b8ab2ec8?productType=146)

All these marvelous goodies are available on the [Spreadshirt shop](https://shop.spreadshirt.fr/solarus-labs). Have fun while walking around the shelves of our little shop!

[![banner](images/spreadshirt-banner.png)](https://shop.spreadshirt.fr/solarus-labs)
