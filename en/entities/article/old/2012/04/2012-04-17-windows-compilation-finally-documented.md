A few days ago, I received an e-mail asking how I proceed to compile Solarus under Windows. That's because the compilation instructions were very incomplete. As I made a quite detailed answer, this problem is fixed: the <a title="Compilation instructions" href="http://www.solarus-games.org/solarus/compilation-instructions/">compilation instructions</a>�now explain how I compile Solarus with Windows.

In short, I compile Solarus with Code::Blocks and MinGW32, i.e. in a Unix environment. I don't use CMake, but it should also work with it.�It would also be interesting to try Visual Studio.

Anyway, the hard part is to install all the required libraries. This is not specific to Solarus and I can't really help you much with that� Any software with dependencies on other libraries is a nightmare to compile with Windows. You must download the headers and the libraries on each website, hoping that the website provides the library in a binary form. If it does not, you have to compile it yourself (or for Windows 32-bit, use the ones I have already compiled for you :)).

With any Linux distribution, downloading and installing the libraries is totally trivial. One command line or one click, as you wish. But if you are using Windows, you probably have a good reason: you like <span style="text-decoration: line-through;">pain</span> challenge! Therefore, why don't you consider moving to the next step: compile for Mac OS X? This will clearly be another dimension of challenge.

More seriously, what I mean is that installing a Linux distribution in dual-boot next to your Windows system is much, much easier than getting all dependencies right and compiling in Windows.

(And I don't even talk about Mac OS X - I keep this topic for a future post.)