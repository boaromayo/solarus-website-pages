I have just reorganized the git repository of Solarus. Our quests are now in separated git repositories. The main repository (<a href="https://github.com/christopho/solarus">https://github.com/christopho/solarus</a>) no longer contains quests. It contains the engine, the map editor and some other tools.

Here are the new repositories for our quests:
<ul>
	<li>Zelda Mystery of Solarus DX:�<a href="https://github.com/christopho/zsdx">https://github.com/christopho/zsdx</a></li>
	<li>Zelda Mystery of Solarus XD:�<a href="https://github.com/christopho/zsxd">https://github.com/christopho/zsxd</a></li>
	<li>Zelda Mercuris' Chest:�<a href="https://github.com/christopho/zelda_mercuris_chest">https://github.com/christopho/zelda_mercuris_chest</a></li>
</ul>
What? Zelda Mercuris' Chest? What is that?

It is our new project in development! More details will come soon� But here is a first screenshot:

<img class="aligncenter" title="Mercuris' Chest - Rail Temple" src="/images/zmc_mine_2.png" alt="" width="320" height="240" />

&nbsp;