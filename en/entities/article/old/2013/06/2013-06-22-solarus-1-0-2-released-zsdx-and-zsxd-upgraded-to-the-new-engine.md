An update of Solarus 1.0, Zelda Mystery of Solarus DX and Zelda Mystery of Solarus XD is available!

This update fixes a bunch of bugs of previous versions. It is recommended to upgrade.

Because it is a patch release (1.0.1 to 1.0.2), there is no change in the format of data files or in the Lua API, everything remains compatible.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/downloads/download-solarus/">Download Solarus 1.0.2</a></li>
	<li><a title="Solarus Quest Editor" href="http://www.solarus-games.org/solarus/quest-editor/">Solarus Quest Editor</a></li>
	<li><a href="http://www.solarus-games.org/doc/1.0.0/lua_api.html">Lua API documentation</a></li>
</ul>
<div>

And our games Zelda Mystery of Solarus DX and Zelda Mystery of Solarus XD are now fully working with Solarus 1.0. They are both released in a new version 1.6. There is also a new title screen from Neovyse (thanks!) and the English translation of both games has been greatly improved (thanks ibara!).

We have worked hard to upgrade both games to the new Lua API (thanks for your help pdewacht!). This does not change much things for the players, but if you are developping a quest with Solarus, you might be interested in reusing and modifying some of our scripts, like the HUD, the pause menu, the title screen or some enemies. The Lua scripting API of Solarus 1.0 allows to customize all of this!

</div>
<h2>Changes in Solarus 1.0.2</h2>
<ul>
	<li>Fix a crash when a treasure callback changes the hero's state (#224).</li>
	<li>Fix a crash when a victory callback changes the hero's state.</li>
	<li>Fix a crash due to invalid sprite frame when animation is changed (#26).</li>
	<li>Fix an assertion error with FollowMovement of pickables.</li>
	<li>Fix the fullscreen mode not working on Mac OS X 10.7+ (#213, #220).</li>
	<li>Fix pickable treasures that could be obtained twice sometimes.</li>
	<li>Fix fade-in/fade-out effects on sprites that did not work (#221).</li>
	<li>Fix sol.audio.play_music() that failed with "none" or "same" (#201).</li>
	<li>Fix item:set_sound_when_brandish() that did not work.</li>
	<li>Fix diagonal movement that could bypass sensors since Solarus 1.0.1.</li>
	<li>Fix circle movement not working after entity:set_enabled(true).</li>
	<li>Fix detection of movement finished for NPCs.</li>
	<li>Fix memory issues with menus (#210).</li>
	<li>Fix handling of nil parameter in boolean setters (#225).</li>
	<li>Fix hangling the default language.</li>
	<li>Correctly suspend timers when set_suspended_with_map is called.</li>
	<li>When a sprite is suspended, suspend its transitions (#226).</li>
	<li>Don't die if a music or a sound cannot be found.</li>
	<li>Don't die if an image cannot be found.</li>
	<li>Don't die if running a savegame without starting location specified.</li>
	<li>Don't die if a script refers to a non-existing equipment item.</li>
	<li>Don't die if the self parameter is missing when calling a method (#219).</li>
	<li>Fix dangling pointers after removing some kind of entities.</li>
</ul>
<h2>Changes in Solarus Quest Editor 1.0.2</h2>
<ul>
	<li>Allow to create map entities from the quest tree (#208).</li>
	<li>Fix a typo in the bomb flower sprite (#214).</li>
	<li>Fix a possible NullPointerException when opening an invalid map.</li>
</ul>
<h2>Documentation changes</h2>
<ul>
	<li>Add the syntax specification of maps and tilesets.</li>
</ul>
<h2>Changes in Zelda Mystery of Solarus DX 1.6</h2>
<ul>
	<li>Improve the English translation.</li>
	<li>New title screen from Neovyse (#45).</li>
	<li>Fix a typo in French dialogs.</li>
	<li>Fix a door bug that could get the hero stuck in empty rooms.</li>
	<li>Fix saving the state of Billy's door.</li>
	<li>Dungeon 1: keep the final door open when coming back in the boss room.</li>
	<li>Dungeon 7 2F: save the nort-west torches.</li>
	<li>Savegames menu: the joypad was not working to delete a savegame.</li>
	<li>Dungeon 10 B1: fix a cauldron the hero could walk on (#20).</li>
	<li>Rupee house: don't let the player to get the piece of heart with the sword.</li>
	<li>Dungeon 6 2F: move crystal blocks to avoid a possible breach.</li>
	<li>Outside world B3: fix a minor tile error (#12).</li>
	<li>Dungeon 8 B1: fix a minor tile error (#2).</li>
	<li>Dungeon 8 B3: fix a minor tile error.</li>
</ul>
<h2>Changes in Zelda Mystery of Solarus XD 1.6</h2>
<ul>
	<li>Improve the English translation.</li>
	<li>Fix the hero stuck after jumping on a block in dungeon 2.</li>
	<li>Fix all missing empty lines in dialogs.</li>
	<li>Add the Solarus engine logo at startup.</li>
</ul>