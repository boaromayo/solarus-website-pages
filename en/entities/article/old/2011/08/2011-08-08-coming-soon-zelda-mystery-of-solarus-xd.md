Yes, the title is correct: Zelda Mystery of Solarus XD, not DX! DX is in progress too, but this is not what I'm talking about today!

Zelda Mystery of Solarus XD (or ZSXD for short) is one of our creations. It's a small parodic game that we released on April 1st, 2011. I did not talk about it earlier on this blog because the game was only available in French. However, people from the <a href="http://en.wikipedia.org/wiki/Caanoo">Caanoo</a> French-speaking community are currently working on a Caanoo port of this parodic game ZSXD. And they decided to translate it in English. We helped them for the translation, and it's almost done! It still needs some final verifications.

Once it's done, we will be able to make a release of ZSXD in English! Though it's a parodic mini-game (initially a big April 1st joke), it's a real, full game with two huge dungeons and 5-10 hours of playing.

Here are some screenshots:

<a href="/images/yoda.png"><img class="aligncenter size-full wp-image-180" title="Yoda" src="/images/yoda.png" alt="" width="320" height="240" /></a>

<a href="/images/long_text.png"><img class="aligncenter size-full wp-image-178" title="Long text" src="/images/long_text.png" alt="" width="320" height="240" /></a>
<a href="/images/creeper.png"><img class="aligncenter size-full wp-image-177" title="Creeper!" src="/images/creeper.png" alt="" width="320" height="240" /></a>
<a href="/images/strike.png"><img class="aligncenter size-full wp-image-179" title="Strike" src="/images/strike.png" alt="" width="320" height="240" /></a>
<a href="/images/cannonballs.png"><img class="aligncenter size-full wp-image-176" title="Cannonballs" src="/images/cannonballs.png" alt="" width="320" height="240" /></a>Stay tuned ;)