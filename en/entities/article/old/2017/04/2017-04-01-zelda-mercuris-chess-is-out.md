<img class="aligncenter wp-image-1505 size-medium" src="/images/zsxd2_title@2x-300x249.png" alt="" width="300" height="249" />

The incredible just happened.

Today we are able to offer you, and waaaaaay before the announced release date of 2023, the final version of our new Zelda game: Mercuris Chess !

This is not without some emotion and proudness that we give you the opportunity to download our creation from now.

&nbsp;

<img class="aligncenter wp-image-1506 size-medium" src="/images/zsxd2_box-300x214.jpg" alt="" width="300" height="214" />

&nbsp;
<h4 style="text-align: center;"><a href="http://www.solarus-games.org/games/zelda-xd2-mercuris-chess/">Download Zelda Mercuris Chess (full game)</a></h4>
<p style="text-align: center;">Caution:
French only, Windows only (for the moment)</p>
<p style="text-align: left;">This ultimate version was made with long development nights, intense mapping, lots of coffee and Schoko-bons. We hope that you will enjoy this new 2D adventure which will make you travel through marvelous lands, wander complex dungeons and perhaps evoke some memories.</p>
<p style="text-align: left;"><img class="aligncenter wp-image-1507 size-medium" src="/images/artwork_link-300x258.png" alt="" width="300" height="258" /></p>
<p style="text-align: left;">Some of you may have guessed that a release was close, since Christopho published regularly screenshots of the game on his Twitter account since the begining of the year. Tell us, if you were right !</p>
<p style="text-align: left;"><img class="aligncenter wp-image-1508 size-medium" src="/images/artwork_grump-292x300.png" alt="" width="292" height="300" /></p>
<p style="text-align: left;">Don't hesitate to give us your appreciation in the comments or on the forum about this new adventure that closes years and years of work and passion.</p>
<p style="text-align: left;">Last but not least, we have to pay hommage to the whole Solarus Team who worked relentlessly on this crazy project. We can be proud of what we accomplished, guys ! Thank you all for the Game Jam memories, and congratulations for finishing this project.</p>
<p style="text-align: left;">See you and have fun !</p>