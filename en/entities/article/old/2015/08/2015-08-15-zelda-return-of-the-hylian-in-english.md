<em>Zelda : Return of the Hylian</em> is now available in <strong>English</strong>!

The translation was imported from the original game that was already available in English. This is a beta translation, please report us any mistake. Enjoy!
<ul>
	<li><a href="http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/">Download</a></li>
</ul>
Also, you can now decorate your desktop with a <em>Return of the Hylian</em> wallpaper (1920x1080 px, ask for more sizes):

[caption id="attachment_1191" align="aligncenter" width="300"]<a href="http://www.solarus-games.org/wp-content/uploads/2015/08/roth_wallpaper_1920x1080.jpg"><img class="wp-image-1191 size-medium" src="/images/roth_wallpaper_1920x1080-300x169.jpg" alt="Wallpaper (1920x1080 px)" width="300" height="169" /></a> Wallpaper (1920x1080 px)[/caption]