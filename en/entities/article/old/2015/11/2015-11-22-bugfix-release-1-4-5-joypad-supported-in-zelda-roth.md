A new bugfix release of Solarus, Solarus Quest Editor and our games was just published!

A bunch of bugs were fixed. <a href="http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/">Zelda Return of the Hylian</a> can now be played with a joypad, and commands can be customized!

<img class="aligncenter" src="/images/CSLQAbHWoAAy572.png:large" alt="" width="640" height="480" />

Also, <a href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/">Zelda Mystery of Solarus DX</a> is now available in Italian (beta) thanks to Marco!
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/engine/download/">Download Solarus 1.4</a></li>
	<li><a href="http://www.solarus-games.org/engine/solarus-quest-editor/">Solarus Quest Editor</a></li>
	<li><a title="LUA API documentation" href="http://www.solarus-games.org/doc/latest/lua_api.html">Lua API documentation</a></li>
</ul>
<h2>Changes in Solarus 1.4.5</h2>
<ul>
	<li>Fix file name not shown when there is an error in dialogs file (#718).</li>
	<li>Fix saving special characters in data files (#719).</li>
	<li>Fix sol.main.load_file() returning a string instead of nil on error (#730).</li>
	<li>Fix performance issue when sprites have huge frame delays (#723).</li>
	<li>Fix collisions triggered for removed entities (#710).</li>
	<li>Fix hero disappearing if lifting animation has less than 5 frames (#682).</li>
	<li>Fix collisions with diagonal dynamic tiles larger than 8x8 (#486).</li>
	<li>Fix path finding movement not working with NPCs (#708).</li>
	<li>Fix stuck on non-traversable dynamic tiles covered by traversables (#769).</li>
	<li>Fix collision detection of custom entities that do not move.</li>
	<li>Fix pickables with special movement falling in holes too early.</li>
	<li>Fix blocking streams not working when the hero's speed is greater (#488).</li>
</ul>
<h2>Changes in Solarus Quest Editor 1.4.5</h2>
<ul>
	<li>Add keyboard shortcut (F4) to switch between map view and map script (#75).</li>
	<li>Map editor: fix entity being moved after closing its dialog (#76).</li>
	<li>Map editor: start selection when clicking a tile with control/shift (#47).</li>
	<li>Map editor: synchronize tile patterns selection from map selection (#35).</li>
	<li>Sprite editor: improve auto-selection after removing a direction (#70).</li>
	<li>Dialogs/strings editor: add a duplicate button (#72).</li>
	<li>Dialogs/strings editor: Shows also missing marks on parent nodes (#68).</li>
	<li>Fix crash on FreeBSD when running the quest (#112).</li>
	<li>Fix crash on Mac OS X 64 bit.</li>
</ul>
<h2>Changes in Zelda ROTH SE 1.0.7</h2>
<ul>
	<li>Allow to customize keyboard and joypad controls (#2).</li>
	<li>Add title screen music (#74).</li>
	<li>Add left/right arrows in pause menus to indicate possible actions (#60).</li>
	<li>Show the current floor when it changes.</li>
	<li>Fix typo in French dialogs in the intro.</li>
	<li>Fix the blue potion dialog in English (#78).</li>
	<li>Fix entities falling in holes even when hooked to the hookshot.</li>
	<li>Fix caps lock state ignored after restarting the game (#20).</li>
	<li>Fix hero stuck in a cliff near shadow village.</li>
	<li>Fix hammer stakes pushed too early (#65).</li>
	<li>Fix enemies active in other regions before taking the first separator (#72).</li>
	<li>Fix enemy projectiles not removed when taking a separator (#71).</li>
	<li>Fix saving with life 0 during the game-over sequence (#82).</li>
	<li>Fix collisions of fire rod and ice rod (#55).</li>
	<li>Fix torches staying on after game over.</li>
	<li>Fix weak cavern wall already open in south-east.</li>
	<li>Make the hammer activate crystals (#75).</li>
	<li>Improve moving the camera with Ctrl+direction (#34).</li>
	<li>Remember the last savegame played.</li>
	<li>Dungeon 5: fix falling hands not properly removed.</li>
	<li>Dungeon 7: fix a door not correctly aligned.</li>
	<li>Dungeon 9: fix door closed when leaving without Zelda after boss key (#79).</li>
	<li>Dungeon 9: fix boss door reopening after killing him (#81).</li>
	<li>Dungeon 9: update soldier sprites to the original ones.</li>
</ul>
<h2>Changes in ZSDX 1.10.2</h2>
<ul>
	<li>Add Italian translation (thanks Marco).</li>
	<li>Make fairies play a sound when restoring life even when already full (#101).</li>
	<li>Fix dialog cursor drawn at wrong position after successive questions (#105).</li>
	<li>Dungeon 1: fix unintentional extra difficulty with block alignment (#102).</li>
	<li>Dungeon 3: fix enemy entering mini-boss room (#107).</li>
	<li>Dungeon 9: fix spikes sending back to wrong room (#108).</li>
	<li>Dungeon 10: fix evil tiles door not opening sometimes (#94).</li>
	<li>End screen: fix freezing the hero (#109).</li>
</ul>
<h2>Changes in ZSXD 1.10.2</h2>
<ul>
	<li>Make fairies play a sound when restoring life even when already full (#101).</li>
</ul>
Enjoy!