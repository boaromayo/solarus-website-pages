<div id="LC59" class="line">

A bugfix release named Solarus 1.2.1 is available!

It addresses a few issues with recent features that were not working correctly, like custom entities, and a problem of corrupted image when creating a new quest with the editor.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/download/">Download Solarus 1.2</a></li>
	<li><a title="Solarus Quest Editor" href="http://www.solarus-games.org/development/quest-editor/">Solarus Quest Editor</a></li>
	<li><a title="LUA API documentation" href="http://www.solarus-games.org/doc/latest/lua_api.html">Lua API documentation</a></li>
</ul>
<h2>Changes in Solarus 1.2.1</h2>
</div>
<ul>
	<li>Fix entity:is_in_same_region() giving wrong results (#500).</li>
	<li>Fix custom_entity:set_can_traverse() giving opposite results.</li>
	<li>Fix custom_entity:on_interaction() not always called.</li>
	<li>Fix custom_entity sprite collision issues with some entities (#536).</li>
	<li>Fix a crash in enemy:restart() when the enemy is dying (#558).</li>
	<li>Fix hero:set_tunic_sprite_id() resetting the direction to right (#511).</li>
	<li>Fix timer:get_remaining_time() always returning 0 (#503).</li>
	<li>Fix declaring global variables from a map script (#507).</li>
	<li>Fix the hero sometimes moving while no keys are pressed (#513). By xethm55.</li>
	<li>Fix on_joypad events not always working (#519). By xethm55.</li>
	<li>Add an error when a hero sprite animation is missing (#485). By Nate-Devv.</li>
</ul>
<h2>Changes in Solarus Quest Editor 1.2.1</h2>
<ul>
	<li>Fix corrupted image in quest created by Quest &gt; New quest (#548).</li>
	<li>Fix tiles created on invisible layer (#508). By Maxs1789.</li>
	<li>Fix crash when an NPC sprite does not have 4 directions (#510). By Maxs1789.</li>
</ul>
<div id="LC80" class="line"></div>