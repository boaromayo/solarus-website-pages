### Presentation

*The Legend of Zelda: The Minish Cap*, publié en 2004 sur Game Boy Advance, est le dernier épisode de Zelda en 2D. Il dispose d'un pixel art somptueux, et d'un gameplay riche, faits d'éléments repris des épisodes en 3D et d'autres totalement nouveaux, telle que la capacité de réduire sa taille à celle d'une souris ! Malgré la faible longueur de sa quête principale, la force du jeu réside dans sa direction artisitique pleine de charme et extrêmement influente.

Ce pack de ressources a été commencé par [Gregory McGregerson](https://github.com/GregoryMcGregerson) mais a actuellement besoin d'aide de la part d'artistes ou développeurs pour être terminé. Rejoignez-nous sur [le forum anglophone](http://forum.solarus-games.org/index.php/topic,1346.0.html) pour nous aider !

![The Legend of Zelda: The Minish Cap box](images/box_art.png)
