### Présentation

Il y a quelques, un utilisateur de la communauté Zelda Classic appelé ZoriaRPG m'a contacté pour réaliser un tileset Creative Commons pour la prochaine version du logiciel. Au lieu d'utiliser des graphismes propriétaires Zelda, tout serait à refaire avec des designs originaux. Malheureusement, je n'ai jamais été capable de mener ce projet à terme. Donc voici ce que j'ai fini, au cas où cela pourrait être utile à quelqu'un.

J'ai fait de mon mieux pour organiser les choses, supprimer le contenu violant le copyright et rendre les images aussi compactes que possible. Il y a assez de tiles pour faire des cartes du monde avec de l'herbe, de la forêt et des environnements urbains. Le travail sur le monde souterrain n'a pas été fini, et une vaste majorité des sprites inclus n'ont pas d'animation.

Les animations de tiles sont placées en lignes horizontales et les tiles similaires sont groupés ensemble. Le tileset a été entièrement fait dans une palette NES, donc c'est possible de recolorer les choses pour avoir des environnements avec des couchers de soleil, de la nuit, de la neige, des marécages, etc. Les fichiers dans le dossier "aseprite" sont des animations avec calques, qui peuvent être ouvertes dans Aseprite.
