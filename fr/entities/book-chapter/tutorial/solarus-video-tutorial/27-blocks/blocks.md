# {title}

[youtube id="KOaKe5G3LbA"]

## Sommaire

- Utiliser les entités blocs
  - Ne pouvoir pousser un bloc qu'une seule fois
  - Pousser un bloc autant que possible
  - Tirer un bloc
  - Contraindre les directions de tirage/poussage
  - Pousser un bloc sur un interrupteur uniquement activé par bloc
- Utiliser les blocs dans le script de map
  - Faire apparaître un coffre quand un interrupteur est activé, et le faire disparaître le cas échéant
  - Verrouiller l'interrupteur après que le coffre ait été ouvert

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
- [Ressources utiles pour reproduire ce chapitre](https://www.solarus-games.org/tuto/fr/basics/ep16_ressources.zip)
