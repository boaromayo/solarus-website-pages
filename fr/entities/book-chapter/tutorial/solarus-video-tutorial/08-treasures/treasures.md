# {title}

[youtube id="8pJ9nedg9Qw"]

## Sommaire

- Créer un script d'objet
- Utiliser l'éditeur de dialogues
- Objets ramassables
  - Cœur
- Les coffres
  - L'épée
- Objets desctructibles
  - Le bouclier
- Les sprites d'objets

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
- [Ressources utiles pour reproduire ce chapitre](https://www.solarus-games.org/tuto/fr/basics/ep8_ressources.zip)
