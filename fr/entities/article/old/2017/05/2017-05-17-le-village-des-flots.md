Zelda Mercuris' Chest se complète peu à peu ! Aujourd'hui, Newlink vous propose cette image tirée du charmant village maritime que notre héros va avoir l'occasion de visiter.

<a href="../data/fr/entities/article/old/2017/05/images/zora.png"><img class="aligncenter size-medium wp-image-38181" src="/images/zora-300x226.png" alt="zora" width="300" height="226" /></a>

Vous aurez ici l'occasion de croiser un peuple bien connu de la série des Zelda ;)

&nbsp;