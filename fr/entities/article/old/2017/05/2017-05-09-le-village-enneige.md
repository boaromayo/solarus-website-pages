Le monde de Mercuri's Chest est vaste, on vous l'avait annoncé. Aujourd'hui, Newlink vous présente cette toute capture d'écran toute fraîche du village dans les montagnes !

<a href="../data/fr/entities/article/old/2017/05/images/snow.png"><img class="aligncenter size-medium wp-image-38174" src="/images/snow-300x227.png" alt="snow" width="300" height="227" /></a>

Les maps des diverses régions avancent petit à petit. Le désert, les forêts, la ville, le parc et autres. On est ambitieux pour ce projet, on a plein d'idées et ça prend forme peu à peu !

&nbsp;