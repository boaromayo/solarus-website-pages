Alors que récemment nous vous parlions de la grande ville qui petit à petit se remplit de nombreux personnages, aujourd'hui nous allons nous intéresser à ce qu'il se passe dans une des maisons qui composent cette cité.

<strong>Mercuris' Chest </strong>possèdera son lot de personnages inédits et spécifiquement conçus pour ce nouvel opus. Cependant nous ne dévoilerons ni l'identité de ce personnage ni ses motivations, à vous de le découvrir lors de votre aventure. ;)

<a href="../data/fr/entities/article/old/2017/12/images/intérieurmaisonMC.png"><img class="alignnone size-medium wp-image-38226" src="/images/intérieurmaisonMC-300x225.png" alt="" width="300" height="225" /></a>

A bientôt sur Zelda Solarus pour de prochaines nouvelles.