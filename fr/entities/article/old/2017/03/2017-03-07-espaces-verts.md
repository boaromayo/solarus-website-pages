Les architectes de la grande ville de Zelda Mercuris' Chest ont pris soin de prévoir des espaces verts. Aujourd'hui Link emprunte un pont qui emjambe un petit bassin.

<a href="../data/fr/entities/article/old/2017/03/images/town_small_bridge.png"><img class="aligncenter size-medium wp-image-38107" src="/images/town_small_bridge-300x225.png" alt="town_small_bridge" width="300" height="225" /></a>

Petite information : la ville contiendra des lieux calmes comme des lieux très animés, avec de nombreuses mini-quêtes !