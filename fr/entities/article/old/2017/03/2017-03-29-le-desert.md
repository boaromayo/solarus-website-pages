Aujourd'hui on vous dévoile un tout petit peu plus de détails sur le désert de Zelda Mercuris' Chest, qui est pour ne rien vous cacher une de mes maps préférées.

<a href="../data/fr/entities/article/old/2017/03/images/desert.png"><img class="aligncenter size-medium wp-image-38124" src="/images/desert-300x225.png" alt="desert" width="300" height="225" /></a>

Le projet avance de plus en plus, on travaille en équipe sur plusieurs maps et donjons en parallèle. À la semaine prochaine !