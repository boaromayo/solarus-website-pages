Aujourd'hui il est temps de vous présenter un autre donjon. Réalisé par Metallizer, il promet d'être original et mémorable. On vous en dévoilera un peu plus dans les prochaines semaines.

<a href="../data/fr/entities/article/old/2017/01/images/lighthouse_chest.png"><img class="aligncenter size-medium wp-image-38083" src="/images/lighthouse_chest-300x225.png" alt="lighthouse_chest" width="300" height="225" /></a>

&nbsp;

Notez que Newlink nous propose ici un nouveau graphisme pour le coffre :)