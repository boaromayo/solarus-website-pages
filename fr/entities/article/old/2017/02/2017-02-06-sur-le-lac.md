Aujourd'hui on vous livre une capture d'écran d'une région dont on ne vous a pas encore beaucoup parlé : le grand lac !

<a href="../data/fr/entities/article/old/2017/02/images/lake.png"><img class="aligncenter size-medium wp-image-38089" src="/images/lake-300x225.png" alt="lake" width="300" height="225" /></a>

&nbsp;

Situé au nord-est de la ville principale, on y trouve quelques îles reliées par des ponts. On a hâte de vous faire visiter tout ça !