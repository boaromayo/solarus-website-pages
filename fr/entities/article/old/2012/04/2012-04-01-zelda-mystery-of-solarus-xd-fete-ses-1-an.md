<p>Notre jeu parodique <a href="http://www.zelda-solarus.com/jeu-zsxd-download">Zelda Mystery of Solarus XD</a> est disponible y a un an exactement. Il a été téléchargé un peu plus de 14000 fois.</p>
<div style="text-align: center"><p><img src="/images/boite_400.jpg" /></p></div>
<p>Pour fêter ça, Binbin et moi avons décidé d'y rejouer et de nous enregister en vidéo ;)</p> <p>Nous avons donc refait le jeu complètement, il y en a pour 2h16 de vidéo au total ! :D</p> <p>Si vous n'y avez encore jamais joué, par exemple parce que vous avez téléchargé Zelda Solarus DX sans connaître Zelda Solarus XD, nous ne pouvons que vous conseiller de le <a href="http://www.zelda-solarus.com/jeu-zsxd-download">télécharger</a> ^_^. Autrement, ces vidéos vont vous rappeler des souvenirs.</p>
<p style="text-align: center"><iframe width="425" height="349" src="http://www.youtube.com/embed/fsJmAweBXVw?hl=fr&fs=1" frameborder="0" allowfullscreen></iframe></p>
<p style="text-align: center">
<iframe width="425" height="349" src="http://www.youtube.com/embed/WxUeMsF8DTc?hl=fr&fs=1" frameborder="0" allowfullscreen></iframe></p>
<p>À vos sabres laser !</p>