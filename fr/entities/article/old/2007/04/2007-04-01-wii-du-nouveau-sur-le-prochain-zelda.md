Depuis quelque temps, des rumeurs circulaient au sujet d'un Zelda en préparation sur Wii alors même que Twilight Princess était encore en phase de finalisation. Aujourd'hui, nous en apprenons davantage sur cet opus. En effet, nous apprenons que le prochain Zelda utilisera les graphismes de Twilight Princess, et la seconde information dévoilée, c'est que cet épisode sera jouable via la connexion Wi-Fi Nintendo, puisqu'il s'agit en fait d'un opus à la Four Swords. Et il semblerait que l'aventure ne peut être faite qu'à quatre, donc seuls les possesseurs d'une connexion wifi seront intéressés, il semblerait aussi qu'il y a des mini jeux, tout comme PH.  
Ce que je peux dire, c'est que ce Zelda paraîtra moins obscurs que Twilight Princess.

On peut penser que le moteur sera peut-être basé sur Twilight Princess, mais il se pourrait qu'il y ait quelques améliorations.
De ce que j'ai pu en tirer, c'est que cet opus nous réserve son lot d'originalité tant par le scénario, que par le game play puisqu'on nous promet des énigmes inédites, un méchant différent des autres connus... Mais ce qui est important de noter, c'est qu'il fera bien la transition entre Twilight Princess et TWW et nous expliquera entres autre comment Hyrule s'est retrouvée ensevelie sous l'eau et comment Ganondorf est revenu.

Voici un artwork que j'ai réussi à voler au cours de mon entretien afin d'en savoir plus sur cet opus et en exclusivité pour Zelda-Solarus.

[url=http://img148.imageshack.us/img148/3637/artzeldawiinj8.jpg]Artwork Zelda Wii[/url]

Mais pour le moment, nous n'en savons pas énormément, c'est tout ce que j'ai pu tirer de la part de la mafia (j'ai faillit même y laisser des plumes :o)... Mais je pense que c'est déjà pas mal.