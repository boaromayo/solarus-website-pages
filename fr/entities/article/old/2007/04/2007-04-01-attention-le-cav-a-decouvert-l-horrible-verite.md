Les Zoras n'ont aucun pouvoir véritable ! Ce ne sont que des usurpateurs ! Le Christopho que nous connaissons et qui a donné naissance à Zelda-Solarus est mort. En réalité, ce n'est pas Christopho, mais le fantôme de Mitterand !

Le CAV a découvert l'horrible vérité, et ont décidé de reprendre la relève !

[center][img]http://img117.imageshack.us/img117/8590/seblechepw6.jpg[/img][/center]

Je vous préviens, partisans de Marco, nous sommes armés ! Nous étendons notre pouvoir à travers cette communauté, et ce afin de réaliser notre idéal de paix, d'amour et de justice !

Rejoignez notre cause ! Non à la tyrannie !