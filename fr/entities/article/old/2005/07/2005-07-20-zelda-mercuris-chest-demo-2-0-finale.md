<p>Voilà, avec un peu de retard la démo 2.0 de notre projet Zelda : Mercuris' Chest est enfin disponible en version finale ! De nombreux bugs ont été corrigés depuis la version bêta sortie le 26 avril. Nous vous conseillons de télécharger cette version finale et de l'installer à la place de la version bêta.</p>

<p>Parmi les principales corrections apportées par rapport à la version bêta, notons que cette version finale prend en charge les manettes de jeu et que le système des collisions entre Link et les ennemis a été amélioré. La liste complète des corrections est indiquée sur le <a href="http://forums.zelda-solarus.com/index.php/topic,8237.0.html">forum</a>.</p>

<p><a href="http://www.zelda-solarus.com/jeux.php?jeu=zmc&zone=download">Télécharger la démo 2.0</a></p>

<p>La sortie de cette version marque donc un tournant dans le développement du projet, puisque nous allons maintenant attaquer le jeu complet pour de bon. Nous vous donnerons régulièrement des nouvelles. Et de nouveaux artworks réalisés par Neovyze seront très bientôt publiés !</p>