Bonjour à tous.

En raison d'un problème indépendant de notre volonté, nous avons été contraints de désactiver temporairement le site habituel à la suite d'une attaque. À la place, voici un site provisoire de secours le temps de régler le problème.

Une aide mystérieuse nous est parvenue sous forme de message :

<i>« Toujours par deux ils vont. Ni plus, ni moins. Un premier et un second. Que la Forcetri soit avec vous. »
</i>
<a href="../data/fr/entities/article/old/2013/04/images/artwork_yoda_kapoera.jpg"><img class="alignnone size-medium wp-image-32600 aligncenter" alt="Yoda" src="/images/artwork_yoda_kapoera-300x219.jpg" width="300" height="219" /></a>

Selon les informations que nous avons ainsi obtenues, tout serait lié à ce qu'il s'est passé il y a 7 ans :

<a href="../data/fr/entities/article/old/2013/04/images/artwork_super_tomate.jpg"><img class="alignnone size-medium wp-image-32601 aligncenter" alt="Super Tomate" src="/images/artwork_super_tomate-300x229.jpg" width="300" height="229" /></a>

Super Tomate est de retour ! Malheureusement, nous ne savons pas sa destination, ni avec quelle force extra-terrestre il traite. Rassurez-vous : il sera traqué, sans relâche, et servira d'ingrédient principal pour notre prochaine pizza.

Ce qui est sûr, c'est qu'à cause de cette attaque nous avons dû mettre en place ce site web provisoire étant donné que l'ancien est en mauvaise posture.

Veuillez nous excuser pour la gêne occasionnée. Nous tentons de rétablir la situation au plus vite, en attendant nous espérons que cette version dégradée du site ne vous dérangera pas trop.

En dédommagement, nous vous dévoilons la vérité sur une blague qui a duré trop longtemps : l'annulation de Zelda Mercuris' Chest, il y a exactement 6 ans, était un poisson d'avril :) True story!

<img class="alignnone aligncenter" alt="" src="/images/zmc_mine_1.png" width="320" height="240" /> <img class="alignnone aligncenter" alt="" src="/images/zmc_lighthouse_2.png" width="320" height="240" />