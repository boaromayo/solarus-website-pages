<p>Salut à tous !</p>

<p>Une petite update juste pour vous dire que malgré le manque de mises à jour je ne laisse pas le site à l'abandon, et encore moins le jeu !</p>

<p>Si vous êtes en manque d'infos inédites, visitez régulièrement le <a href="http://www.zelda-solarus.com/forum.php3">Forum</a>, il s'y passe pas mal de choses en ce moment.</p>

<p>Pour ce qui est de l'avancement du jeu, je suis toujours dans la programmation du donjon 5, même si je m'occupe toujours de quelques détails extérieurs. Je ne peux pas vous en dire plus :)</p>