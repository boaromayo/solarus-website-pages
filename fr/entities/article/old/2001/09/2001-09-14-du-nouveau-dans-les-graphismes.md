<p>Salut à tous,</p>

<p>Ces derniers jours le jeu s'est assez amélioré graphiquement. J'ai refait complètement le chipset de la Plaine d'Hyrule. Ne vous inquiétez pas, je n'ai absolument rien supprimé ! J'ai en quelque sorte optimisé le chipset, pour pouvoir mettre le plus possible de graphismes dedans. J'y ai passé des heures et c'est enfin fini !!</p>

<p>J'ai ainsi rajouté de nouveaux graphismes tirés de Zelda 3 : les rivières (qui remplacent ainsi celles des RTP), les ponts et surtout le Château d'Hyrule ! Ce qui peut vous laisser imaginer tous les scnéarios que vous voulez pour la suite du jeu...</p>