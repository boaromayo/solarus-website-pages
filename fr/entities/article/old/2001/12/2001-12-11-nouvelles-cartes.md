<p>Pour faire que ce jeu soit encore plus beau, on a refait les cartes et boussoles des donjons ! C'est beaucoup plus soigné ! D'ailleurs, voici deux exemple, avant et après la modification...</p>
<table border="0" width="100%" cellpadding="5">
  <tr>
    <td width="100%" align="center">
      <h2><img border="0" src="/images/cbvert02.png" width="320" height="160" align="left">AVANT</h2>
      <p>Toutes plates avec quand même les légendes et le curseur qui désigne
      l'endroit où se trouve Link.</p>
    </td>
  </tr>
</table>
<table border="0" width="100%" cellpadding="5">
  <tr>
    <td width="100%" align="center">
      <h2> <img border="0" src="/images/cbvert0.png" width="320" height="160" align="left">APRES</h2>
      <p>Plus beau avec un fond granulé ! L'image ci-contre est en taille
      réelle ! Ce genre de graphismes a été fait avec Paint Shop Pro ! Voila
      pour l'info !</p>
    </td>
  </tr>
</table>