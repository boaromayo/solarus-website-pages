Voici une petite news pour vous tenir au courant du non avancement de notre projet Zelda : Mercuris' Chest.
Je ne vais pas y aller par quatre chemins : le projet n'a quasiment pas avancé depuis qu'on est en 2006. Ce n'est pas par manque de motivation ou par manque d'idées. C'est à cause de mes études. Depuis début janvier, j'ai sans arrêt des examens et surtout des échéances de projets à rendre. Le rythme de travail est assez soutenu et il m'est simplement impossible de consacrer du temps au site et notamment à Zelda M'C. A moins de travailler la nuit :D

Le problème, c'est que ça ne va pas s'arranger tout de suite. Je risque d'être encore être très occupé jusqu'au 21 avril. Après cette date, la plupart des projets pour mes études seront terminés, et donc ça ira beaucoup mieux :)

Concernant Zelda M'C, le développement est donc en quelque sorte en pause depuis début janvier. Newlink fait quelques graphismes de temps en temps, quand je lui en réclame :p Mais comme je ne fais presque rien de mon côté, je n'ai pas beaucoup de travail à lui donner...

L'avenir de Zelda M'C, c'est donc :
[list]
[li]Rien de nouveau pendant encore un mois[/li]
[li]Le redémarrage du projet à partir du 21 avril[/li]
[li]Par la suite, j'aurai beaucoup plus de temps libre, je n'aurai plus de période aussi chargée qu'en ce moment.[/li]
[/list]

Développer un tel projet est une tâche qui demande beaucoup de temps, beaucoup de motivation et beaucoup de travail. Ce n'est pas pour rien que la plupart des projets de jeux amateurs n'aboutissent pas. Quand on commence, on ne se doute pas du temps et de l'investissement que ça demande. Le moteur du jeu est en développement depuis 3 ans et demi. Je n'ai aucune idée de la date à laquelle le jeu lui-même sera terminé. Nous ne sommes pas des professionnels, nous ne travaillons pas à plein temps et donc vous comprendrez bien pourquoi c'est si long.

On vous demande donc encore de la patience. Comme je l'ai toujours dit, le jeu sortira un jour, c'est une certitude. On ne va pas annuler le projet après 3 ans de travail acharné. On a un moteur de jeu puissant, un super scénario et une équipe motivée. Alors vous pouvez compter sur nous pour donner le meilleur de nous-mêmes à partir du 21 avril et jusqu'à la fin du développement.