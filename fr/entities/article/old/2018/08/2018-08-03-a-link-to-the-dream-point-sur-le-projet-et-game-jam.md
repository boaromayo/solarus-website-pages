Par cette chaleur intense et en période de vacances d'été, il est temps pour nous de faire le point sur le <strong>projet A Link to the Dream</strong>, notre beau<strong> remake de Link's Awakening</strong> !
Nous n'avons pas beaucoup donné de nouvelles dernièrement mais sachez que le projet avance toujours, notamment au niveau des ennemis. Dans le jeu original, il y a en a une centaine au moins et graphiquement ils sont presque tous finis, revisités en version A Link to the Past ! Bravo et merci à Julien pour son énorme boulot sur les sprites.

Niveau mapping, c'est presque terminé. Il restera encore des corrections mais dans l'ensemble, nous avons presque tout mis en place. Pour la partie extérieure, il nous reste encore la zone de la descente des rapides et pour la partie intérieure, il nous reste surtout à corriger des erreurs de mapping.

Niveau quête principale, cela avance aussi puisque nous pouvons presque jouer au jeu jusqu'aux dunes de Yarna. Cela suit donc son cours.

Niveau donjons, nous travaillons d'arrache-pied dessus et nous promettons d'ailleurs quelques surprises.

Enfin, nous travaillons aussi beaucoup pour améliorer le visuel global du jeu. Chaque détail compte et pas mal de surprises sont aussi au rendez-vous.

<a href="/images/gj.png"><img class="aligncenter wp-image-38341 size-full" src="/images/gj.png" alt="" width="660" height="300" /></a>

Mais vous aurez l'occasion aussi d'en savoir plus ce week end, puisqu'une game jam spéciale "A Link to the Dream" va avoir lieu. Nous allons en effet nous réunir afin d'avancer un maximum sur le projet ! Ce sera aussi l'occasion de mettre en place quelques streamings.

Sachez qu'en parallèle, la version 1.6 du moteur Solarus avance. Christopho profite de nos avancées sur A Link to the Dream pour corriger des bugs sur l'éditeur et le moteur de manière à ce que l'on ait un outil le plus stable et le plus complet possible. Et je peux vous dire que cela promet !!!