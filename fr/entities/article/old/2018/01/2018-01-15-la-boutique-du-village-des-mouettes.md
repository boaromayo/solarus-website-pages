Comme vous le savez, nous travaillons actuellement sur un projet de <strong>remake de Link's Awakening nommé A Link to the Dream</strong>. Ce projet, qui est réalisé en partenariat avec <a href="https://www.zeldaforce.net">Zeldaforce</a>, utilise les <strong>graphismes de A Link to the Past</strong> et est à un stade bien avancé. En effet, nous travaillons actuellement d'arrache-pied sur ce jeu qui s'annonce vraiment prometteur.

Ces derniers temps, nous avons avancé sur la fameuse boutique du village des Mouettes et son marchand maléfique. Voilà un petit aperçu du jeu en espérant que cela vous plaise.
<a href="../data/fr/entities/article/old/2018/01/images/DTWLq-BW4AAdFJ5.jpg"><img class="alignnone size-medium wp-image-38239" src="/images/DTWLq-BW4AAdFJ5-300x238.jpg" alt="" width="300" height="238" /></a>

Nous comptons poster régulièrement différentes captures d'écran afin de vous donner des nouvelles régulières du projet.

Enfin, sachez qu'en ce moment Christopho travaille aussi sur le projet au niveau du mapping. D'ailleurs, ce soir à <strong>21h00</strong>, vous aurez l'occasion de suivre son travail en direct sur <strong>Youtube</strong>. Pour suivre, rien de plus simple, il suffit de cliquer sur ce <a href="https://www.youtube.com/watch?v=9TAgC_GnZCc">lien</a>.

Sachez que vous aurez aussi la possibilité de suivre le stream directement sur la page d'accueil de Zelda Solarus.