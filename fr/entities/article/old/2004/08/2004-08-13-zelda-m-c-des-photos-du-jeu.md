<p>Nous sommes en pleine période de vacances mais chez l'équipe ZS, le mot "vacances" n'a pas la même signification que pour ceux qui se dorent la pillule au soleil.</p>
<p>De nombreuses choses ont été intégrées au moteur du jeu ces dernières semaines, nous bénéficions d'un inventaire quasi terminé et la plupart des objets que nous inclurons dans le jeu ont été listés. Quoi ? Vous vous moquez de ce que je dis ? Les photos ? Mouais, il faut bien rassasier les paparazzi que vous êtes alors consommez ces captures avec modération parce que ce sont les premières du jeu complet.</p>
<p align=center>
<a href=../data/fr/entities/article/old/2004/08/images/develop001.png target=_blank><img src=/images/develop001_mini.png width=200 height=150 border=0></a> 
<a href=../data/fr/entities/article/old/2004/08/images/develop002.png target=_blank><img 
src=/images/develop002_mini.png width=200 height=150 border=0></a><br>
<a href=../data/fr/entities/article/old/2004/08/images/develop003.png target=_blank><img src=/images/develop003_mini.png width=200 height=150 border=0></a> 
<a href=../data/fr/entities/article/old/2004/08/images/develop004.png target=_blank><img src=/images/develop004_mini.png width=200 height=150 border=0></a><br>
<a href=../data/fr/entities/article/old/2004/08/images/develop005.png target=_blank><img src=/images/develop005_mini.png width=200 height=150 border=0></a></p>
<p>Ce sont donc les images du moteur, il n'a aucun vrai décor, tout est pour les tests de collision, de positionnement, de vitesse, d'affichage...</p>
<p>Vous remarquez aussi sur la photo du menu des options que vous pourrez bien configurer vos propres touches, utiliser une manette ou un joystick, décider de jouer dans une fenêtre ou en plein écran bref, le joueur est roi !</p>
<p>Prochaines infos très bientôt, restés connectés !</p>