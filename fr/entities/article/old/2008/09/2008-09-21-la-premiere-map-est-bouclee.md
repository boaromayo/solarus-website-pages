Notre projet Zelda : Mystery of Solarus DX avance bien !
Aujourd'hui, je suis heureux de vous annoncer que la première map du jeu, la maison de Link, est entièrement terminée ! ^_^

[center][img]http://www.zelda-solarus.com/images/zsdx/first_map.png[/img][/center]
Cela signifie que tous les éléments qui se trouvent dessus sont programmés. Autrement dit, le jeu est maintenant capable de gérer les coffres, preuve à l'appui cette deuxième capture d'écran :

[center][img]http://www.zelda-solarus.com/images/zsdx/shield.png[/img][/center]
On peut donc placer facilement des coffres sur les maps et choisir leur contenu :). Et comme vous le voyez, les coffres ne sont pas la seule nouveauté depuis la dernière news puisque le système de messages a lui aussi été développé. C'est à peu de choses près le même que dans Mercuris' Chest. Autre nouveauté : la gestion de l'inventaire, c'est-à-dire l'écran qui affiche les objets que vous possédez, et la possibilité d'assigner deux de ces objets aux icônes comme dans Ocarina of Time. Link ne peut pas encore les utiliser, mais il peut déjà les assigner aux deux icônes. Actuellement, je suis en train de programmer les autres écrans du menu de pause : la carte, le statut de la quête et les options. En attendant, voici déjà l'écran d'inventaire : :mrgreen:

[center][img]http://www.zelda-solarus.com/images/zsdx/inventory.png[/img][/center]
Même si tous les objets n'apparaissent pas ici, je tiens à remercier l'ensemble de l'équipe, qui a participé de manière constructive à la critique et au relookage des icônes d'objets de l'inventaire ^_^.

C'est tout pour aujourd'hui, à bientôt pour d'autres nouvelles ! ;)