La rubrique consacrée à Zelda Solarus DX commence peu à peu à se remplir ! Aujourd'hui, deux premières pages sont disponibles. Il s'agit d'abord de la galerie d'images, qui rassemblera toutes les captures d'écran que nous dévoilerons au cours du développement. Elle contient déjà les images publiées le 1er avril.

[list]
[li][url=http://www.zelda-solarus.com/jeux.php?jeu=zsdx&amp;zone=images]Galerie d'images[/url][/li]
[/list]

La deuxième page est consacrées à l'équipe de développement. Elle détaille les personnes qui contribuent au projet et leurs rôles :).

[list]
[li][url=http://www.zelda-solarus.com/jeux.php?jeu=zsdx&amp;zone=equipe]L'équipe[/url][/li]
[/list]

A très bientôt pour d'autres nouvelles. Je vous expliquerai notamment ce qui a été fait jusqu'à présent pour vous indiquer où en est le développement du jeu ^_^.