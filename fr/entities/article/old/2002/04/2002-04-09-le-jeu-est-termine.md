<p>Nous sommes fiers de vous annoncer Zelda : Mystery of Solarus est terminé depuis cet après-midi !</p>

<p>Les testeurs sont actuellement en train de faire leur boulot et ont 16 jours pour finir le jeu en trouvant le maximum de bugs.</p>

<p>N'oubliez pas le grand rendez-vous du 26 avril, avec la sortie de la version finale !</p>