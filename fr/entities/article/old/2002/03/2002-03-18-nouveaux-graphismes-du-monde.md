<p>Salut à tous !</p>

<p>J'ai refait les chipsets de la carte du monde, car les anciens graphismes n'avaient pas les vraies couleurs de Zelda 3. Ils étaient trop contrastés et trop sombres. Voici le résultat :</p>


<table border="0" cellpadding="15" cellspacing="0" align="center">
<tr align="center">
<td><p class=Centre>Avant :</p></td>
<td><a href="/images/63.png" target="_blank"><img src="/images/63.png" width="160" border="0"></a></td>
</tr>

<tr align="center">
<td><p class=Centre>Après :</p></td>
<td><a href="/images/62.png" target="_blank"><img src="/images/62.png" width="160" border="0"></a></td>
</tr></table>