<p>Salut à tous !</p>

<p>Les problèmes de connexion au chat semblent avoir été réglés et tout le monde a pu participer au quizz ! Les scores on été très serrés et tout s'est joué sur les dernières questions. Voici le classement définitif :</p>

<p><ul type=disc>
<li><b>1er</b> : flobo avec 22 points</li>
<li><b>2e</b> : Anthony Stabile avec 21 points</li>
<li><b>3e</b> : Link-Sheik avec 12 points</li>
<li><b>4e</b> : snake la terreur avec 9 points</li>
<li><b>5e</b> : Benny avec 8 points</li>
<li><b>5e ex-aequo</b> : Oxyd avec 8 points</li>
<li><b>7e</b> : Kafei avec 6 points</li>
<li><b>8e</b> : billy bob avec 5 points</li>
<li><b>9e</b> : leed avec 3 points</li>
<li><b>10e</b> : Djeepy46234 avec 1 point</li>
<li><b>10e ex-aequo</b> : S-Premier avec 1 point</li>
<li><b>10e ex-aequo</b> : Jayman avec 1 point</li>
<li><b>10e ex-aequo</b> : aurelien1234 avec 1 point</li>
</ul></p>

<p>Voilà, on espère que vous vous êtes bien amusés ! Bravo à flobo qui recevra la démo de ZAP en avant-première quand elle sera finie, et bravo à mega bowser et à Link-Sheik qui recevront quant à eux le CD des musiques de ZS. Et merci à tous d'avoir participé !</p>