<p>Il y en a beaucoup qui se sont fait avoir ! Zelda : Advanced Project ne sortira pas le 30 septembre mais largement avant, rassurez-vous. Nous fixerons bientôt la vraie date si tout va bien, comme je le disais dans la mise à jour de ce matin.</p>

<p>Et oui nous sommes le premier avril et nous n'avons pas pu résister à l'envie de vous faire une blague... En tout cas, bravo à mega bowser qui a été le premier à comprendre et à nous dire qu'il s'agissait d'un poisson d'avril. En récompense il aura le droit de tester la démo de ZAP en avant-première :-) Désolé pour les autres, ça s'est joué à pas grand chose !</p>

<p>Si vous avez cru à cette histoire, et bien sachez qu'il ne faut pas toujours croire tout ce qu'on vous dit ;-) L'histoire de faire la démo en plusieurs langues n'était qu'une invention, elle sortira uniquement en français. Mais pour ce qui est du jeu complet on en reparlera.</p>

<p>Rendez-vous au premier avril 2004 pour essayer de ne pas tomber dans le panneau la prochaine fois !</p>