<p>Avec un peu de retard, les forums sont enfin à nouveau disponibles ! Nous les avons déménagés sur un compte sur free.fr afin de diminuer les ressources utilisées par notre site sur le serveur. Contrairement à la dernière fois, tous les messages ont été conservés !</p>

<p>La nouvelle adresse des forums est <a href="http://zeldasolarus.free.fr/forums" target="_blank">http://zeldasolarus.free.fr/forums</a> mais l'adresse <a href="http://www.zsforums.fr.st" target="_blank">http://www.zsforums.fr.st</a> marche aussi.</p>

<p><a href="http://zeldasolarus.free.fr/forums" target="_blank">Visiter les forums</a></p>