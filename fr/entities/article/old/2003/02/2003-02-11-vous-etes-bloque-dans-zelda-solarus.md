<p>Depuis que nous avons supprimé la soluce du site, on reçoit plein d'e-mails de personnes bloquées dans Zelda : Mystery of Solarus. C'était prévisible, mais comme j'ai eu pitié de vous, j'accepte finalement de vous aider.</p>

<p>Nous n'avons cependant pas remis en ligne la soluce, car nous ne voulons pas gâcher la durée de vie du jeu. A la place de la soluce, nous avons donc fait une page qui répertorie les endroits où vous êtes le plus fréquemment bloqués, d'après les e-mails que vous nous envoyez et les messages du forum.</p>

<p>Pour chacun de ces endroits, on vous donne une indication qui vous aidera un peu. Un petit coup de pouce en quelque sorte :-)</p>

<p><a href="http://www.zelda-solarus.net/jeux.php?jeu=zs&zone=deblocage">Déblocage Zelda Solarus</a></p>