<p>Le changement d'hébergeur que nous vous annoncions la semaine n'a pas encore pu avoir lieu à cause d'un problème lié au transfert du nom de domaine. L'adresse www.zelda-solarus.net ne fonctionne plus pour le moment et il faudra attendre environ trois semaines avant que tout ne rentre dans l'ordre.</p>

<p>En attendant, nous avons transféré le site sur consolemul, et l'adresse du site devient provisoirement http://zelda-solarus.consolemul.com ou http://www.zelda-solarus.fr.st qui redirige vers la première.</p>

<p>Tout le site est censé fonctionner. Les forums sont également à nouveau ouverts. Nos adresses e-mail habituelles ne fonctionnent plus pendant ces trois semaines. Mais vous pouvez toujours nous contacter en cliquant <a href="http://www.zelda-solarus.com/mailto:christopho128@yahoo.fr">ici</a>.</p>

<p>Tout cela est provisoire. Dans trois semaines, le site déménagera pour de bon vers le nouvel hébergeur, l'url www.zelda-solarus.net marchera et nos adresses e-mail normales aussi. Et il n'y aura plus aucune pub :)</p>